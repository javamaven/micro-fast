package com.micro.fast.ucenter.dao;

import com.micro.fast.ucenter.pojo.UcenterUserDetails;

public interface UcenterUserDetailsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UcenterUserDetails record);

    int insertSelective(UcenterUserDetails record);

    UcenterUserDetails selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UcenterUserDetails record);

    int updateByPrimaryKey(UcenterUserDetails record);
}