package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsUserOrganization;

public interface UpmsUserOrganizationMapper {
    int deleteByPrimaryKey(Integer userOrganizationId);

    int insert(UpmsUserOrganization record);

    int insertSelective(UpmsUserOrganization record);

    UpmsUserOrganization selectByPrimaryKey(Integer userOrganizationId);

    int updateByPrimaryKeySelective(UpmsUserOrganization record);

    int updateByPrimaryKey(UpmsUserOrganization record);
}