package com.micro.fast.upms.dao;

import com.micro.fast.upms.pojo.UpmsSystem;

public interface UpmsSystemMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UpmsSystem record);

    int insertSelective(UpmsSystem record);

    UpmsSystem selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UpmsSystem record);

    int updateByPrimaryKey(UpmsSystem record);
}