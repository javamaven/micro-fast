package com.micro.fast.upms.pojo;

import java.io.Serializable;

public class UpmsOrganization implements Serializable {
    private Integer id;

    private Integer pid;

    private String name;

    private String description;

    private Long ctime;

    private static final long serialVersionUID = 1L;

    public UpmsOrganization(Integer id, Integer pid, String name, String description, Long ctime) {
        this.id = id;
        this.pid = pid;
        this.name = name;
        this.description = description;
        this.ctime = ctime;
    }

    public UpmsOrganization() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", pid=").append(pid);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", ctime=").append(ctime);
        sb.append("]");
        return sb.toString();
    }
}